import pytest
from src.main import sum_two_numbers 

@pytest.mark.parametrize("num1, num2, expected_sum", [
    (1,1,2),
    (2,2,4),
    (4,4,8)
])
def test_sum_two_numbers(num1, num2, expected_sum):
    assert sum_two_numbers(num1, num2) == expected_sum


@pytest.mark.skip(reason="Dummy test for CI to have skipped tests")
@pytest.mark.parametrize("num1, num2, expected_sum", [
    (1,1,2),
    (2,2,4),
    (4,4,8)
])
def test_dummy_test_with_skip(num1, num2, expected_sum):
    assert sum_two_numbers(num1, num2) == expected_sum